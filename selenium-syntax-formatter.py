import sublime
import sublime_plugin
import subprocess
import os
import tempfile
import sys
import pipes

reload(sys)
sys.setdefaultencoding('utf8')


class SeleniumSyntaxCommand(sublime_plugin.TextCommand):

    def run(self, edit):
        if (sublime.platform() != "linux"):
            sublime.error_message("This plugin supports Linux only.")
            return False

        current_view = self.view
        file_name = os.path.basename(current_view.file_name())

        extension = os.path.splitext(current_view.file_name())[1]
        if (extension not in (".txt", ".inc")):
            sublime.error_message("This plugin only works on .txt or .inc files.")
            return False

        body = current_view.substr(sublime.Region(0, current_view.size()))

        success_message = 'Couldn\'t format test ' + file_name

        try:
            tmpTestFile = tempfile.NamedTemporaryFile(delete=False)
            tmpTestFile.write(body)
            tmpTestFile.close()

            command = sublime.packages_path() + "/MaglabsSeleniumSyntaxFormatter/maglabs-test-syntax.rb " + tmpTestFile.name
            process = subprocess.Popen(command, stdout=subprocess.PIPE, stderr=subprocess.STDOUT, shell=True)
            output = process.stdout.read()

            os.unlink(tmpTestFile.name)

        except Exception:
            output = body

        current_view.erase(edit, sublime.Region(0, current_view.size()))
        current_view.insert(edit, 0, output)
        sublime.status_message(success_message)
