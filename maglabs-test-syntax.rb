#!/usr/bin/env ruby
@min_spaces = 50

def clean_command line
  command = /^(?<command>\:\w+\:)\s+(?<params>.+)/.match(line)
  return "#{command['command'].ljust(@min_spaces)}#{command['params']}"
end

$<.each do |line|
  line.gsub!(/\t/, ' ')
  if (/^\:\w+\:\s+./ =~ line) # :command:      Parameters
    puts clean_command(line)
  elsif (/^\s+./ =~ line)  #                   Additional Parameters
    line.strip!
    @min_spaces.times {print ' '}
    puts line
  else # Everything Else
    puts line
  end
end


